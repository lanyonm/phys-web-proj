var uuid = {
  "id": "gonna-have-to-be-set-somehow",
  "type": "display"
};
var socket = io();
// socket.emit('new-client', uuid);
socket.on('sendGuid', function(guid) {
  uuid.id = guid;
  socket.emit('displayConnection', guid);
});
socket.on('question', function(msg) {
  $('#question').text(msg);
});
socket.on('answers', function(msg) {
  $('#answers').empty();
  data = [];
  for (var i = 0; i < msg.length; i++) {
    // console.log(msg[i]);
    $('#answers').append('<li id="' + msg[i].id + '">' + msg[i].text + ' - ' + msg[i].votes + '</li>');
    data.push(msg[i].votes);
  }
  $('.chart').empty();
  var width = 420;
  var barHeight = 20;
  var x = d3.scale.linear()
      .domain([0, d3.max(data)])
      .range([0, width]);
  var chart = d3.select(".chart")
      .attr("width", width)
      .attr("height", barHeight * data.length);
  var bar = chart.selectAll("g")
      .data(data)
      .enter().append("g")
      .attr("transform", function(d, i) { return "translate(0," + i * barHeight + ")"; });
  bar.append("rect")
      .attr("width", x)
      .attr("height", barHeight - 1);
  bar.append("text")
      .attr("x", function(d) { return x(d) - 3; })
      .attr("y", barHeight / 2)
      .attr("dy", ".35em")
      .text(function(d) { return d; });
});
