// would like to gather this from a cookie
var uuid = {
  "id": 'some-sort-of-uuid',
  "type": "client"
};

var socket = io();
socket.emit('new-client', uuid);

socket.on('question', function(msg) {
  $('#question').text(msg);
});

socket.on('answers', function(msg) {
  $('#answers').empty();
  for (var i = 0; i < msg.length; i++) {
    var li = '<li id="' + msg[i].id + '">' + msg[i].text;
    li += ' - <button type="button" class="btn btn-link" data-answer-id="';
    li += msg[i].id + '">vote!</button></li>';
    $('#answers').append(li);
  }
  addEventHandlers();
});

function addEventHandlers() {
  $('#answers li button').each(function(index, listItem) {
    $(listItem).on('click', function(event) {
      socket.emit('answer-picked', {
        'uuid': uuid.id,
        'answerId': this.dataset.answerId
      });
    });
  });
}
