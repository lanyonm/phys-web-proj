$(function() {
  $('#questions').empty();
  $.ajax({
    url: '/poll-admin/questions',
    method: "get",
    dataType: 'json',
    success: function(data) {
      for (var i = 0; i < data.length; i++) {
        $('#questions').append('<li id='+data[i].id+'>'+data[i].text+'</li>');
      }
    }
   });
  $('#answers').empty();
  $.ajax({
    url: '/poll-admin/answers',
    type: "get",
    dataType: 'json',
    success: function(data) {
      for (var i = 0; i < data.length; i++) {
        var li = '<li id='+data[i].id+'><span>'+data[i].text+'</span> | ';
        li += '<button type="button" class="btn btn-link">edit</button></li>';
        $('#answers').append(li);
      }
      attachEditHandlers();
    }
  });
});

function attachEditHandlers() {
  $('#answers li button').each(function(index, listItem) {
    $(listItem).on('click', function(event) {
      var li = $(this).parent();
      var span = li.children('span');
      var val = span.html();
      span.remove();
      li.prepend('<input type="text" value="'+val+'"/>');
      var button = li.children('button').remove();
      li.append('<button type="button" class="btn btn-primary save">save</button> ');
      li.append('<button type="button" class="btn btn-danger cancel">cancel</button></li>');
      attachSaveCancelHandlers();
    });
  });
};

function attachSaveCancelHandlers() {
  $('.save').each(function(index, listItem) {
    $(listItem).on('click', function(event) {
      var id = $(this).parent().attr('id');
      var val = $(this).parent().children('input').val();
      console.log('going to save '+val+' to '+id);
      $.ajax({
        url: '/poll-admin/answers/save',
        method: 'post',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({'id': id, 'val': val}),
        success: function() {
          console.log('yay!');
        },
        error: function() {
          console.log('boo!');
        }
      })
    });
  });
};
