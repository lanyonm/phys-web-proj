'use strict';

var eddystoneBeacon = require('eddystone-beacon');
var ip = require('ip');

var url = 'http://' + ip.address() + '/pc';
console.log('broadcasting ' + url);

eddystoneBeacon.advertiseUrl(url);
