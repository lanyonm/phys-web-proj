'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var router = express.Router();
var cuid = require('cuid');
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static('./public'));
app.use(bodyParser.json());
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});
app.get('/pc', function(req, res){
  res.sendFile(__dirname + '/poll-client.html');
});
app.get('/poll-client', function(req, res){
  res.sendFile(__dirname + '/poll-client.html');
});
app.get('/poll-display', function(req, res){
  res.sendFile(__dirname + '/poll-display.html');
});

var questions = [
  {
    'id': 1,
    'text': 'What\'s your favorite food?'
  }
];
var answers = [
  {
    'id': 1,
    'text': 'apples',
    'votes': 0
  },
  {
    'id': 2,
    'text': 'cereal',
    'votes': 0
  },
  {
    'id': 3,
    'text': 'cake',
    'votes': 0
  },
  {
    'id': 4,
    'text': 'broccoli',
    'votes': 0
  }
];

var displays = {
  displaySockets: {},
  getAllSockets: function() {
    return displays.displaySockets;
  },
  getSocketByGuid: function (guid) {
    return displays.displaySockets[guid];
  },
  generateGuid: function() {
    return cuid();
  }
};

io.on('connection', function(socket){
  var newGuid = displays.generateGuid();
  socket.emit('sendGuid', newGuid);
  socket.emit('question', questions[0].text);
  socket.emit('answers', answers);

  // for displays
  socket.on('displayConnection', function (guid) {
    displays.displaySockets[guid] = socket;
    console.log('registered a display: ' + guid);

    // do other things?

    socket.on('disconnect', function () {
      console.log('deleted a display:    ' + guid);
      delete displays.displaySockets[guid];
    });
  });

  // for voter clients
  socket.on('new-client', function(payload) {
    if (payload.type === 'client')
      console.log('a client connected');
  });
  socket.on('answer-picked', function(msg) {
    console.log(msg.uuid + ' picked ' + msg.answerId);
    answers.forEach(function(answer) {
      if (answer.id == msg.answerId) {
        answer.votes++;
        console.log(answer.text + ' now has ' + answer.votes);
      }
    });
    Object.keys(displays.displaySockets).forEach(function(key) {
      var display = displays.displaySockets[key];
      display.emit('answers', answers);
    });
  });
});

// the admin interface
app.get('/poll-admin', function(req, res) {
  res.sendFile(__dirname + '/poll-admin.html');
});
app.get('/poll-admin/questions', function(req, res) {
  res.json(questions);
});
app.get('/poll-admin/answers', function(req, res) {
  res.json(answers);
});
app.post('/poll-admin/answers/save', function(req, res) {
  console.log('admin has updated '+req.body.id+' to '+req.body.val);
  answers.forEach(function(answer) {
    if (answer.id == req.body.id) {
      answer.text = req.body.val;
    }
  });
  io.emit('answers', answers);
  res.sendStatus(200);
});

var server = http.listen(process.env.PORT || 3000, function(){
  console.log('listening at http://%s:%s', server.address().address, server.address().port);
});
