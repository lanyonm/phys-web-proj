phys-web-proj
------
This is a [Physical Web](http://physical-web.org/) demo in the form of a Node.js project. Currently it runs an Eddystone beacon either on a MacbookPro or a Raspberry Pi. The application served is a mock-polling application where a display prompts users to check the Physical Web where they find the link to the vote on the poll. Websockets are used to make the experience real-time and allow the administrator to distribute changes immediately.

Setup
-----
Node [v4.2.2](https://nodejs.org/dist/v4.2.2/) was used during development, so your mileage may vary using other Node versions.

	npm install

#### Running the beacon

	npm run-script start-beacon

#### Running the server

	npm run-script start-server

Tests
-----
There are no tests at this time. :(

Links
-----

* https://youtu.be/7H_E_ZbFAn0?t=705
* http://www.wadewegner.com/2014/05/create-an-ibeacon-transmitter-with-the-raspberry-pi/
* https://github.com/sandeepmistry/bleno#running-on-linux
* https://github.com/don/node-eddystone-beacon
* http://www.theverge.com/2015/4/20/8454613/push-notifications-android-chrome-42-mobile-web

Raspberry Pi Prerequisites
--------------------------
From a fresh install of Debian Jessie based Raspbian do the following:

* In `raspi-config`
  * Set internationalization options
  * Update hostname
  * Resize boot partition

#### Setup Bluetooth
Install some drivers:

	cd /tmp
	wget www.kernel.org/pub/linux/bluetooth/bluez-5.18.tar.gz
	tar -xvzf bluez-5.18.tar.gz
	cd bluez-5.18
	./configure --disable-systemd
	make
	sudo make install

After a reboot, you'll need to setup the bluetooth device:

	sudo hciconfig hci0 up
	sudo hciconfig hci0 leadv 3
	sudo hciconfig hci0 noscan
